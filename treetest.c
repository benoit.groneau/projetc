#include <stdio.h>
#include <stdlib.h>
#include "directory_lib.h"
#include <dirent.h>
#include <string.h>

#define DATA "./tree.conf"
#define L 60

int main(int argc, char **argv)
{
    FILE *in;
    char s[L];
    int nl = 0;

     if (argc != 2) {
        fprintf(stderr, "Argument manquant, voir readme\n");
        exit(EXIT_FAILURE);
    }

        in = fopen(DATA, "r");
        if (in == NULL)
        {
            perror(DATA);
            return 1;
        }
        while (fgets(s, L - 1, in) != NULL)
        {
            s[strlen(s) - 1] = '\0';
            // printf("%s\n", s);
            if (s[0] != '#')
            {
                char *name = malloc(strlen(s) + 1);
                char *path = malloc(strlen(s) + 1);
                int name_found = 0;
                int cpt_path = 0;
                for (int i = 0; i < strlen(s); i++)
                {
                    if (s[i] == '=')
                    {
                        name_found = 1;
                    }
                    else if (name_found)
                    {
                        path[cpt_path] = s[i];
                        cpt_path++;
                    }
                    else
                    {
                        name[i] = s[i];
                    }
                }
                printf("Recherche de %s dans %s%c%s\n", argv[1], name, '=', path);
                directory_file df = load(path);
                search(argv[1], df);
                free(df);
            }
        }
    fclose(in);
    return 0;
}
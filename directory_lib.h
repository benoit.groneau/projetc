#include <stdio.h>
#include <dirent.h>

struct directory_file {
    char *path;
    struct directory_file *next;
};
typedef struct directory_file *directory_file;

struct directory_file *load(char *path);

struct directory_file *create_directory(char *directory_name, int df_number);

int count_directory_file(char *path);

void afficher_repertoire(directory_file df);


void view_path(char *path, directory_file *df);

void afficher_path(directory_file df);

void inserer(directory_file *df, char *path);

void search(char *string_search ,directory_file df);
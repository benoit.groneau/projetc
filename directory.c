#include "directory_lib.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct directory_file *load(char *path)
{
    directory_file df;
    df = malloc(sizeof(struct directory_file));
    df->path = path;
    df->next = NULL;
    view_path(path, &df);
    return df;
}

void inserer(directory_file *df, char *path)
{
    directory_file temp;
    temp = malloc(sizeof(struct directory_file));
    temp->path = path;
    temp->next = *df;
    *df = temp;
}

void view_path(char *path, directory_file *df)
{
    struct dirent *dirp;
    DIR *t = opendir(path);
    if (t == NULL) {
        printf ("Accés refusés\n");
        // exit(-1);
    }
    else {
        while ((dirp = readdir(t)) != NULL)
    {
        if (!(strcmp(dirp->d_name, ".") == 0 || strcmp(dirp->d_name, "..") == 0))
        {
            char *current_path = (char *)malloc(strlen(path) + 1 * strlen(dirp->d_name) + 2);
            strcpy(current_path, path);
            if (current_path[strlen(current_path) - 1] != '/')
            {
                strcat(current_path, "/");
            }
            strcat(current_path, dirp->d_name);
            inserer(df, current_path);
            if (dirp->d_type == DT_DIR)
            {
                view_path(current_path, df);
            }
        }
    }
    }
    
}

void afficher_path(directory_file df)
{
    directory_file tmp = df;
    printf("%s\n", df->path);
    while ((tmp->next != NULL))
    {
        tmp = tmp->next;
        printf("%s\n", tmp->path);
    }
    printf("\n");
}

void search(char *string_search, directory_file df)
{
    directory_file tmp = df;
    int current_path_size = strlen(tmp->path);
    char *subpath = malloc(current_path_size + 1);
    int cpt = 0;
    for (int i = 0; i < current_path_size; i++)
    {
        // printf("subpath %s\n", subpath);
        // printf("path %s\n", tmp->path);
        // printf("stringsearch %s\n", string_search);
        if (tmp->path[i] == '/')
        {
            if (strlen(string_search) == strlen(subpath))
            {
                if (strlen(string_search) == 1)
                {
                    if (string_search == subpath)
                    {
                        printf("%s\n", tmp->path);
                        for (int j = 0; j < cpt; j++)
                        {
                            subpath[j] = '\0';
                        }
                        break;
                    }
                    else
                    {
                        for (int j = 0; j < cpt; j++)
                        {
                            subpath[j] = '\0';
                        }
                        cpt = 0;
                    }
                }
                else
                {
                    if (strcmp(string_search, subpath) == 0)
                    {
                        printf("%s\n", tmp->path);
                        for (int j = 0; j < cpt; j++)
                        {
                            subpath[j] = '\0';
                        }
                        break;
                    }
                    else
                    {
                        for (int j = 0; j < cpt; j++)
                        {
                            subpath[j] = '\0';
                        }
                        cpt = 0;
                    }
                }
            }
            else
            {
                for (int j = 0; j < cpt; j++)
                {
                    subpath[j] = '\0';
                }
                cpt = 0;
            }
        }
        else
        {
            subpath[cpt] = tmp->path[i];
            if (tmp->path[i + 1] == '\0')
            {
                if (strcmp(string_search, subpath) == 0)
                {
                    printf("%s\n", tmp->path);
                    for (int j = 0; j < cpt; j++)
                    {
                        subpath[j] = '\0';
                    }
                    break;
                }
                else
                {
                    for (int j = 0; j < cpt; j++)
                    {
                        subpath[j] = '\0';
                    }
                    cpt = 0;
                }
            }
            cpt++;
        }
    }
    while ((tmp->next != NULL))
    {
        tmp = tmp->next;
        int current_path_size = strlen(tmp->path);
        char *subpath = malloc(current_path_size + 1);
        int cpt = 0;
        for (int i = 0; i < current_path_size; i++)
        {
            // printf("stringsearch %s\n", string_search);
            if (tmp->path[i] == '/')
            {
                if (strlen(string_search) == strlen(subpath))
                {
                    if (strlen(string_search) == 1)
                    {
                        if (string_search == subpath)
                        {
                            printf("%s\n", tmp->path);
                            for (int j = 0; j < cpt; j++)
                            {
                                subpath[j] = '\0';
                            }
                            break;
                        }
                        else
                        {
                            for (int j = 0; j < cpt; j++)
                            {
                                subpath[j] = '\0';
                            }
                            cpt = 0;
                        }
                    }
                    else
                    {
                        if (strcmp(string_search, subpath) == 0)
                        {
                            printf("%s\n", tmp->path);
                            for (int j = 0; j < cpt; j++)
                            {
                                subpath[j] = '\0';
                            }
                            break;
                        }
                        else
                        {
                            for (int j = 0; j < cpt; j++)
                            {
                                subpath[j] = '\0';
                            }
                            cpt = 0;
                        }
                    }
                }
                else
                {
                    for (int j = 0; j < cpt; j++)
                    {
                        subpath[j] = '\0';
                    }
                    cpt = 0;
                }
            }
            else
            {
                subpath[cpt] = tmp->path[i];
                if (tmp->path[i + 1] == '\0')
                {
                    if (strcmp(string_search, subpath) == 0)
                    {
                        printf("%s\n", tmp->path);
                        for (int j = 0; j < cpt; j++)
                        {
                            subpath[j] = '\0';
                        }
                        break;
                    }
                    else
                    {
                        for (int j = 0; j < cpt; j++)
                        {
                            subpath[j] = '\0';
                        }
                        cpt = 0;
                    }
                }
                cpt++;
            }
        }
    }
}
